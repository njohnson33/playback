#!/bin/sh
##############################################################################
#
# 850 band
#
# 882.75 -- fmin
# 883.98
# 887.67
# 888.90
# 893.10  -- fmax
#
# Calling convention:
#   ./playback0850.sh [ overall_atten_dB<0-31> ]
#
# NOTE:  Must be superuser to run!
#
##############################################################################
export RUNPATH=/opt/playback/P4020_file_transmit/bin
export FILENAME=$RUNPATH/cdma2000_pilot_1x_normal
export CHANS=1
export RATE=1
export BAND=850
export FMIN=882.75
export FMAX=893.10
 
export ATTEN=0
# if number of args < 1, then set atten to 0, otherwise, use value of first arg
if [ $# -ge 1 ]; then
    export ATTEN=$1
fi
 
## 882.75
sudo ${RUNPATH}/P4020FileTransmitTestApp_fixblock --if eth2 --band ${BAND} --srb ${RUNPATH}/cdma_set_exp.srb --board 0 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 882.75 --repeat -1 --atten ${ATTEN} --fmin ${FMIN} --fmax ${FMAX} --daemon
 
## 883.98
sudo ${RUNPATH}/P4020FileTransmitTestApp_fixblock --if eth2 --band ${BAND} --board 0 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start B --center 883.98 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon
 
## 888.90
sudo ${RUNPATH}/P4020FileTransmitTestApp_fixblock --if eth3 --band ${BAND} --board 1 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 888.90 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon
 
## 893.10
sudo ${RUNPATH}/P4020FileTransmitTestApp_fixblock --if eth3 --band ${BAND} --board 1 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start B --center 893.10 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon
 
## 887.67
sudo ${RUNPATH}/P4020FileTransmitTestApp_fixblock --if eth4 --band ${BAND} --board 2 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 887.67 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon


##############################################################################
# Power control sweeping for protiums
#
# Default is 18 dB of attenuation swing
#
# Calling convention:
#    ./powercontrol0850.sh [ attenIndex< 0=>6dB, 1=>12dB, 2=>18dB>, 3=>24dB ]
#
# NOTE:  Must be superuser to run!
#
##############################################################################
export PWR_CTRL_RUNPATH=/opt/playback
# Below times are in ms units
export PWR_CTRL_MINTIME=5000
export PWR_CTRL_MAXTIME=2500

# Default to 18 dB
export PWR_CTRL_ATTEN=3
# if number of args >= 1, use value of first arg
if [ $# -ge 1 ]; then
    export PWR_CTRL_ATTEN=$1
fi

sleep 20

# Power control calling convention:
# P4020_TX_PowerControl <eth> <board: 0-3> <chan: 0-3> <attenParam: 0-3> <mintime> <maxtime>

## 425 - 882.75
# --if eth0 --board 0 --start A
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth2 0 0 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

## 466 - 883.98
# --if eth0 --board 0 --start B
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth2 0 1 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &



##############################################################################
export PWR_CTRL_RUNPATH=/opt/playback
# Below times are in ms units
export PWR_CTRL_MINTIME=2000
export PWR_CTRL_MAXTIME=50000

# Default to 18 dB
export PWR_CTRL_ATTEN=3
# if number of args >= 1, use value of first arg
if [ $# -ge 1 ]; then
    export PWR_CTRL_ATTEN=$1
fi

## 589 - 887.67
# --if eth0 --board 0 --start A
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth4 2 0 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

##############################################################################
export PWR_CTRL_RUNPATH=/opt/playback
# Below times are in ms units
export PWR_CTRL_MINTIME=2000
export PWR_CTRL_MAXTIME=50000

# Default to 18 dB
export PWR_CTRL_ATTEN=2
# if number of args >= 1, use value of first arg
if [ $# -ge 1 ]; then
    export PWR_CTRL_ATTEN=$1
fi

## 630 - 888.90
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth3 1 0 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &


##############################################################################
export PWR_CTRL_RUNPATH=/opt/playback
# Below times are in ms units
export PWR_CTRL_MINTIME=2000
export PWR_CTRL_MAXTIME=50000

# Default to 18 dB
export PWR_CTRL_ATTEN=1
# if number of args >= 1, use value of first arg
if [ $# -ge 1 ]; then
    export PWR_CTRL_ATTEN=$1
fi


## 770 - 893.10
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth3 1 1 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

sleep 10
kill -15 `ps aux | grep "P4020_TX_PowerControl eth3" | grep -v grep | awk '{print $2}'`
kill -15 `ps aux | grep "P4020_TX_PowerControl eth4" | grep -v grep | awk '{print $2}'`

