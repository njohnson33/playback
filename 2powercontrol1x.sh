#!/bin/sh
##############################################################################
# Power control sweeping for protiums
#
# Default is 18 dB of attenuation swing
#
# Calling convention:
#    ./powercontrol0850.sh [ attenIndex< 0=>6dB, 1=>12dB, 2=>18dB>, 3=>24dB ]
#
# NOTE:  Must be superuser to run!
#
##############################################################################
export PWR_CTRL_RUNPATH=/opt/playback
# Below times are in ms units
export PWR_CTRL_MINTIME=1000
export PWR_CTRL_MAXTIME=100000
 
# Default to 18 dB
export PWR_CTRL_ATTEN=3
# if number of args >= 1, use value of first arg
if [ $# -ge 1 ]; then
    export PWR_CTRL_ATTEN=$1
fi
 
 
# Power control calling convention:
# P4020_TX_PowerControl <eth> <board: 0-3> <chan: 0-3> <attenParam: 0-3> <mintime> <maxtime>
 

## 589 - 887.67
# --if eth0 --board 0 --start A
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth4 2 0 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

## 630 - 888.90
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth3 1 0 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

## 770 - 893.10
nohup ${PWR_CTRL_RUNPATH}/P4020_TX_PowerControl eth3 1 1 ${PWR_CTRL_ATTEN} ${PWR_CTRL_MINTIME} ${PWR_CTRL_MAXTIME} >> /tmp/powercontrol.log &

sleep (5)
kill -15 'ps aux | grep "P4020_TX_PowerControl eth3" | grep -v grep | awk '{print $2}'`
kill -15 'ps aux | grep "P4020_TX_PowerControl eth4" | grep -v grep | awk '{print $2}'`


