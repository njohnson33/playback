#!/bin/sh
##############################################################################
#
# 850 band
#
# 882.75 -- fmin
# 883.98
# 887.67
# 888.90
# 893.10  -- fmax
#
# Calling convention:
#   ./playback0850.sh [ overall_atten_dB<0-31> ]
#
# NOTE:  Must be superuser to run!
#
##############################################################################
export RUNPATH=/etc/vanuinc/P4020_file_transmit/bin
export FILENAME=$RUNPATH/cdma_0_freq_offset
export CHANS=1
export RATE=1
export BAND=850
export FMIN=882.75
export FMAX=893.10

export ATTEN=0
# if number of args < 1, then set atten to 0, otherwise, use value of first arg
if [ $# -ge 1 ]; then
    export ATTEN=$1
fi

## 882.75
sudo ${RUNPATH}/P4020FileTransmitTestApp --if eth3 --band ${BAND} --board 0 --srb ${RUNPATH}/cdma_set_exp.srb --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 882.75 --repeat -1 --atten ${ATTEN} --fmin ${FMIN} --fmax ${FMAX} --daemon

## 883.98
sudo ${RUNPATH}/P4020FileTransmitTestApp --if eth3 --band ${BAND} --board 0 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start B --center 883.98 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon

## 888.90
sudo ${RUNPATH}/P4020FileTransmitTestApp --if eth2 --band ${BAND} --board 1 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 888.90 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon

## 893.10
sudo ${RUNPATH}/P4020FileTransmitTestApp --if eth2 --band ${BAND} --board 1 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start B --center 893.10 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon

## 887.67
sudo ${RUNPATH}/P4020FileTransmitTestApp --if eth0 --band ${BAND} --board 2 --chans ${CHANS} --file ${FILENAME} --rate ${RATE} --start A --center 887.67 --repeat -1  --fmin ${FMIN} --fmax ${FMAX} --daemon



